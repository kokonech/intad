## InTAD R package ##

The main target of the package is to detect correlation between expressed genes and selected epigenomic signals i.e. enhancers, differentially methylated regions etc. within topologically associated domains (TADs). Coordinates of TADs can be detected using HiC and other methods.

The analysis starts from collecting signals and genes lying in the same TAD.Then the combined groups in a TAD are analyzed to detect correlations among them. Various parameters can be further controlled. For example, the expression filters, correlation limits (positive,negative), methods (Pearson, Spearman), etc .

Additionally, the sizes of TADs can be changed to find correlation of a gene with a target outside of a TAD or in another TAD. This will allow to detect novel factors connected to tumour development mechanism.

For more details please refer to the tutorial.